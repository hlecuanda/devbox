# NAME

devbox - Convenience skeleton repository for Debian Docker container based projects

# DESCRIPTION

I like to develop projects in separate Docker containers, but got tired of
copy/pasting a lot of the scaffolding when starting. I'll be using `devbox` as
a basis for new project... maybe you should too.

`devbox` will:

    * Build a Docker image
    * Fix Debian so that tmux works
    * Run your bootstrap install script
    * Mount the current directory to /srv/<project-name>

If there's anything else you think would be useful, send a pull request and
i'll be happy to merge.

# INSTALLATION

First, start a new project:

    mkdir my-cool-project
    cd my-cool-project

Next, clone this repo and copy the files to your project
directory:

    git clone https://gitlab.com/alfiedotwtf/devbox.git
    cp devbox/{Dockerfile,Dockerinstall,Makefile} .

Now you can discard the `devbox` clone:

    rm -Rf devbox/

Work on your bootstrap until you're happy:

    vi Dockerinstall

Lastly, build the Docker image:

    make docker-build

*Note:* Each time you modify your `Dockerinstall`, you'll want to rebuild the
Docker image.

# USAGE

Now that everything is setup, it's ready to be used. Each time you want to do
some development in your project's container:

    cd /path/to/my-cool-project
    make docker-run

If everything went smoothly, you should now be within your Debian Docker
container in the `/srv/<project-name>/` directory which is mounted to the
outside world. Neato!

# SUPPORT

Please report any bugs or feature requests at:

* [https://gitlab.com/alfiedotwtf/devbox/issues](https://gitlab.com/alfiedotwtf/devbox/issues)

Feel free to fork the repository and submit pull requests :)

# SEE ALSO

* [Debian](https://www.debian.org/)
* [Docker](https://docs.docker.com/get-started/)

# AUTHOR

[Alfie John](https://www.alfie.wtf)

# WARRANTY

IT COMES WITHOUT WARRANTY OF ANY KIND.

# COPYRIGHT AND LICENSE

Perpetual Copyright (C) to Alfie John

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
