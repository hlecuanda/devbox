CONTAINER_NAME=$(shell pwd | perl -pe 's{.*/}{}')
MOUNT_SRC=$(shell pwd)
MOUNT_DEST=/srv/$(CONTAINER_NAME)

all:
	@echo "targets:"
	@echo "	make docker-build"
	@echo "	make docker-run"

docker-build:
	sed -i s/sed-target/$(CONTAINER_NAME)/ Dockerfile
	docker build -t $(CONTAINER_NAME) .

docker-run:
	$(eval container_id=$(shell docker container ls -a | grep $(CONTAINER_NAME) | cut -d ' ' -f 1 | head -1))
	if [ -z "$(container_id)" ]; then                                                                                           \
	  docker run -it --mount type=bind,source=$(MOUNT_SRC),target=$(MOUNT_DEST) --cap-add SYS_PTRACE $(CONTAINER_NAME) || true; \
	else                                                                                                                        \
	  docker start -ai $(container_id) || true;                                                                                 \
	fi
