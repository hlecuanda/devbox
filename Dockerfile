FROM debian:stretch

# fix apt
RUN apt-get update

# fix environment
RUN apt-get install -y locales tmux git vim
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen

WORKDIR /tmp/
COPY Dockerinstall /tmp/
RUN bash Dockerinstall

WORKDIR /srv/sed-target

CMD /bin/bash
